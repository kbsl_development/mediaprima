/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaPrema.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
//import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;//for encryption
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author lahiru wijesekara
 */

public class MediaPreEghl implements java.io.Serializable {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    private final String url = "https://test2pay.ghl.com/ipgsg/payment.aspx";

    public MediaPreEghl() {

    }

    public String testResponse() {
        String x = "Response TransactionType=SALE&PymtMethod=CC&ServiceID=TV3&PaymentID=BCDGHL20170882018&OrderNumber=OPGJEF20160225009&Amount=10.00&CurrencyCode=MYR&HashValue=7c8d559d32b81f7b7476b9d4ae1bdb73103ac7c0dedad26ecb8484e9c57abae6&TxnID=TV3000BCDGHL20170882018&IssuingBank=HostSimS2S&TxnStatus=0&AuthCode=TV3000&BankRefNo=TV3000BCDGHL20170882018&TxnMessage=Transaction+Successful ";
        return x;
    }

    public String generateUrlParam(String TransactionType, String ServiceID, String PaymentID, String OrderNumber, String PaymentDesc, String MerchantName, String MerchantReturnURL, String Amount, String CurrencyCode, String CardHolder, String CustName, String CustPhone, String CustEmail, String CardNo, String CardExp, String CardCVV2, String CustIP, String LanguageCode, String PymtMethod, String HashValue) {
        String urlpara = MerchantReturnURL
                + TransactionType + ServiceID + PaymentID + OrderNumber + PaymentDesc + MerchantName + Amount + CurrencyCode + CardHolder + CustName + CustPhone + CustEmail + CardNo + CardExp + CardCVV2 + CustIP + LanguageCode + PymtMethod + HashValue;
        return urlpara;
    }
    public String generateHash256(String secret) throws NoSuchAlgorithmException {
        //hash generation algorithem
        String inputString = secret;
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(inputString.getBytes());
        byte byteData[] = md.digest();
        //convert the byte to hexa format
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        //convert the byte to hex 
        StringBuilder hexString;
        hexString = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            String hex = Integer.toHexString(0xff & byteData[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
    public String sendPostData() throws Exception {
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        // reuqest header
        con.setRequestMethod("POST");
        ///con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        String hashKeyValue;
        hashKeyValue = "tv312345TV3ACDGHL20160882017https//payme.com/sucess.jsp10.00MYR175.157.167.764444333322221111";
        // String urlPameters=this.generateUrlParam(url, url, url, url, url, url, url, url, url, url, url, url, url, url, url, url, url, url, url, url)
        String urlParameters = "TransactionType=SALE&ServiceID=TV3&PaymentID=BCDGHL20170882018&OrderNumber=OPGJEF20160225009&PaymentDesc=Bill&MerchantName=Media Prima&MerchantReturnURL=https//payme.com/sucess.jsp&Amount=10.00&CurrencyCode=MYR&CardHolder=jeff&CustName=Jeff Phang&CustPhone=012-6901828&CustEmail=lahirusandaruwan@gmail.com&CardNo=4444333322221111&CardExp=2018&CardCVV2=300&CustIP=175.157.167.76&LanguageCode=EN&PymtMethod=CC&HashValue="+this.generateHash256("lahiru")+"";
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr;
        wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        int responseCode = con.getResponseCode();
        StringBuilder response;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }
        return "Response Code :" + responseCode + "</br> Response " + response.toString();
    }

}
